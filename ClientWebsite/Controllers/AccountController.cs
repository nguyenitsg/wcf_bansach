﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientWebsite.UserService;
using WebSiteBanSach.Models;

namespace WebSiteBanSach.Controllers
{
    public class AccountController : Controller
    {
        NguoiDungServiceClient user = new NguoiDungServiceClient();
        // GET: TaiKhoan
        [HttpGet]
        public ActionResult Register()
        {
            //AccountViewModel convert = new AccountViewModel();
            //KhachHang kh = convert.ConverObjKhachHang(customer);
            //if (user.AddKhachHang(kh))
            //{
            //    ViewBag.TB = "Đăng kí thành công !";
            //}
            //else
            //{
            //    ViewBag.TB = "Đăng kí thất bại !";
            //}
            return View();
        }
        [HttpPost]
        public ActionResult Register(RegisterViewModel customer)
        {
            AccountViewModel convert = new AccountViewModel();
            ClientWebsite.UserService.KhachHang kh = user.getKhachHang(customer.Account.ToString());
            if (kh != null)
            {
                ViewBag.TB = "Account đã tồn tại !";
                return View();
            }
            if (user.AddKhachHang(convert.ConverObjKhachHang(customer)))
            {
                Session["InforLogin"] = convert.ConverObjKhachHang(customer);
                ViewBag.TB = "Đăng kí thành công !";
            }
            else
            {
                ViewBag.TB = "Đăng kí thất bại !";
            }
            //ViewBag.TB = customer.Password;
            return View();
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(LoginViewModel login)
        {
            ClientWebsite.UserService.KhachHang kh = user.getKHLogin(login.Account,login.Password);
            if(kh!= null)
            {
                Session["InforLogin"] = kh;
                if(kh.roles == 1)
                {
                    return RedirectToAction("Index", "Admin");              
                }  
                else
                {
                    if (Session["URL"] != null)
                    {
                        string url = Session["URL"].ToString();
                        Session["URL"] = null;
                        return Redirect(url);

                    }
                    return RedirectToAction("Index", "Home");
                }
            }
            ViewBag.TB = "Account or Password incorrect !";

            
            return View();
        }
        public ActionResult LogOff()
        {
            if (Session["InforLogin"] != null)
            {
                Session["InforLogin"] = null;
                Session["GioHang"] = null;
            }
            return RedirectToAction("Login");
        }
    }
}