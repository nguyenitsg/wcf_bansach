﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientWebsite.BookService;
using PagedList;

namespace WebSiteBanSach.Controllers
{
    public class TopicsController : Controller
    {
        SachServiceClient book = new SachServiceClient();
        // GET: Topics
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult ToPicPartial()
        {
            List<ClientWebsite.BookService.ChuDe> list_cd = book.GetAllCD().ToList();
            return PartialView(list_cd);
        }

        //Sách theo chủ đề
        public ViewResult BooksTopic(int? page,int MaChuDe = 0)
        {
            //Kiểm tra chủ đề tồn tại hay không
            ChuDe cd = book.GetChuDeById(MaChuDe);
            if (cd == null)
            {
                Response.StatusCode = 404;
                return null;
            }
            //Tạo biến số sản phẩm trên trang
            int pageSize = 6;
            //Tạo biến số trang
            int pageNumber = (page ?? 1);
            //Truy xuất danh sách các quyển sách theo chủ đề
            List<Sach> lstSach = book.GetSachByMaCD(cd.MaChuDe).ToList();
            if (lstSach.Count == 0)
            {
                ViewBag.Sach = "Không có sách nào thuộc chủ đề này";
            }
            //Gán danh sách chủ để
            ViewBag.lstChuDe = book.GetAllCD().ToList();
            return View(lstSach.ToPagedList(pageNumber, pageSize));
        }
    }
}