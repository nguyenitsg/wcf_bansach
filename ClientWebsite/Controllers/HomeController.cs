﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ClientWebsite.BookService;
using System.Web.Mvc;
using PagedList;

namespace WebSiteBanSach.Controllers
{
    public class HomeController : Controller
    {
        SachServiceClient book = new SachServiceClient();
        public ActionResult Index(int? page)
        {
            //Tạo biến số sản phẩm trên trang
            int pageSize = 6;
            //Tạo biến số trang
            int pageNumber = (page ?? 1);
            return View(book.getAllSach_Page().ToPagedList(pageNumber, pageSize));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
       

    }
}