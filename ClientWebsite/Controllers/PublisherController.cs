﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientWebsite.BookService;
using PagedList;

namespace WebSiteBanSach.Controllers
{
    public class PublisherController : Controller
    {
        SachServiceClient book = new SachServiceClient();
        // GET: Publisher
        public PartialViewResult _Publishertial()
        {
            List<ClientWebsite.BookService.NhaXuatBan> list_nxb = book.GetAllNXB().ToList();
            return PartialView(list_nxb);
        }
        public ActionResult BooksbyPublisher(int? page, int MaNXB)
        {
            ViewBag.MANXB = MaNXB;
            //Tạo biến số sản phẩm trên trang
            int pageSize = 6;
            //Tạo biến số trang
            int pageNumber = (page ?? 1);
            return View(book.GetSachByNXB(MaNXB).ToPagedList(pageNumber, pageSize));
        }
        public PartialViewResult Tooltip()
        {
            return PartialView(book.getAllSach_Page().ToList());
        }
    }
}