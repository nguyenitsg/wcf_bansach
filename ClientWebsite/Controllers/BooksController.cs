﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientWebsite.BookService;

namespace WebSiteBanSach.Controllers
{
    public class BooksController : Controller
    {
        SachServiceClient book = new SachServiceClient();
        public PartialViewResult BookNewiPartial()
        {
            return PartialView(book.GetTake3Sach().ToList());
        }
        public ViewResult BookDetail(int MaSach = 0)
        {

            Sach sach_detail = book.GetSachById(MaSach);
            if (sach_detail == null)
            {
                //Trả về trang báo lỗi 
                Response.StatusCode = 404;
                return null;
            }
            ClientWebsite.BookService.ChuDe chude = book.GetChuDeById(Int32.Parse(sach_detail.MaChuDe.ToString()));
            ClientWebsite.BookService.NhaXuatBan nxb = book.GetNXBByID(Int32.Parse(sach_detail.MaNXB.ToString()));
            ViewBag.TenChuDe = chude.TenChuDe;
            ViewBag.NhaXuatBan = nxb.TenNXB;
            return View(sach_detail);
        }
        public ActionResult AllBooks(int? page)
        {
            int pageSize = 12;
            //Tạo biến số trang
            int pageNumber = (page ?? 1);
            return View(book.GetSachByMoi().OrderBy(n => n.GiaBan).ToPagedList(pageNumber, pageSize));
        }
    }
}