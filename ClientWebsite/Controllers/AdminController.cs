﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;
using System.Web.Mvc;
using ClientWebsite.UserService;
using ClientWebsite.BookService;
using ClientWebsite.OrderService;
using System.IO;


namespace ClientWebsite.Controllers
{
    public class AdminController : Controller
    {
        NguoiDungServiceClient nd = new NguoiDungServiceClient();
        SachServiceClient sach_client = new SachServiceClient();
        ServiceDonHangClient dh_client = new ServiceDonHangClient();
        // GET: Admin

        // Phần người dùng
        public ActionResult Index()
        {
            List<UserService.KhachHang> kh = nd.getListKhachHang().ToList();
            return View(kh);
        }
        [HttpGet]
        public ActionResult EditUser(int id)
        {
            UserService.KhachHang kh = nd.getKhachHangbyID(id);
            return View(kh);
        }
        [HttpPost]
        public ActionResult EditUser(UserService.KhachHang kh)
        {
            if (nd.EditKhachHang(kh))
            {
                ViewBag.ThongBao = "Cập nhật thành công !";
                return View();
            }
            else
            {
                ViewBag.ThongBao = "Cập nhật thất bại !";
                return View();
            }              
        }
        public ActionResult DetailsUser(int id)
        {
            UserService.KhachHang kh = nd.getKhachHangbyID(id);
            return View(kh);
        }
        [HttpGet]
        public ActionResult DeleteUser(int id)
        {
            UserService.KhachHang kh = nd.getKhachHangbyID(id);
            return View(kh);
        }
        [HttpPost]
        public ActionResult DeleteUser(UserService.KhachHang kh)
        {
            if (nd.RemoveKhachHang(kh.MaKH))
            {

               ViewBag.ThongBao = "Xóa Thành Công !";
            }
            else
            {
                ViewBag.ThongBao = "Xóa TB !"+kh.MaKH;
            }
            return View();
        }

        // Phần sản phẩm
        public ActionResult IndexProduct(int? page)
        {
            int pageSize = 12;
            //Tạo biến số trang
            int pageNumber = (page ?? 1);
            return View(sach_client.getAllSach_Page().ToPagedList(pageNumber, pageSize));
            //List<BookService.Sach> book_list = sach_client.getAllSach_Page().ToList();
            //return View(book_list);
        }

        [HttpGet]
        public ActionResult EditProduct(int id)
        {
            BookService.Sach sach_id = sach_client.GetSachById(id);

            ViewBag.Image = sach_id.AnhBia;
            ViewBag.MaChuDe = new SelectList(sach_client.GetAllCD().ToList().OrderBy(n => n.TenChuDe), "MaChuDe", "TenChuDe", sach_id.MaChuDe);
            ViewBag.MaNXB = new SelectList(sach_client.GetAllNXB().ToList().OrderBy(n => n.TenNXB), "MaNXB", "TenNXB", sach_id.MaNXB); 
            return View(sach_id);
        }
        [HttpPost]
        public ActionResult EditProduct( string url_img, BookService.Sach sach, HttpPostedFileBase fileAnhBia)
        {
            ViewBag.MaChuDe = new SelectList(sach_client.GetAllCD().ToList().OrderBy(n => n.TenChuDe), "MaChuDe", "TenChuDe", sach.MaChuDe);
            ViewBag.MaNXB = new SelectList(sach_client.GetAllNXB().ToList().OrderBy(n => n.TenNXB), "MaNXB", "TenNXB", sach.MaNXB);
            if (fileAnhBia == null)
            {
                sach.AnhBia = url_img;
                sach_client.UpdateSach(sach);
                //ViewBag.ThongBao = url_img + "11111";

                return RedirectToAction("EditProduct", "Admin", new { id = sach.MaSach });
            }
            else
            {
                //Thêm vào cơ sở dữ liệu
                if (ModelState.IsValid)
                {
                    //Lưu tên file

                    var fileName = Path.GetFileName(fileAnhBia.FileName);
                    //Lưu đường dẫn của file
                    var path = Path.Combine(Server.MapPath("~/HinhAnhSP"), fileName);
                    //Kiểm tra hình ảnh đã tồn tại chưa
                    fileAnhBia.SaveAs(path);
                    sach.AnhBia = fileAnhBia.FileName;
                    sach_client.UpdateSach(sach);
                    //ViewBag.ThongBao = "ton tai";
                    return RedirectToAction("EditProduct", "Admin", new { id = sach.MaSach });
                }
            }
          
            return View();
        }
         [HttpGet]
        public ActionResult DeleteProduct()
        {

            return View();
        }
         [HttpPost]
        public ActionResult DeleteProduct(BookService.Sach sach)
        {

            return View();
        }
        public ActionResult DetailsProduct(int id)
        {
            BookService.Sach sach = sach_client.GetSachById(id);
            return View(sach);
        }
    }
}