﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientWebsite.BookService;

namespace WebSiteBanSach.Controllers
{
    public class SearchController : Controller
    {
        SachServiceClient book = new SachServiceClient();
        // GET: Search
       
        [HttpPost]
        public ActionResult SearchResults(FormCollection f, int? page)
        {
            string sLoai = f["droplist"].ToString();
            string sTuKhoa = f["txtTimKiem"].ToString();
            ViewBag.TuKhoa = sTuKhoa;
            int pageNumber = (page ?? 1);
            int pageSize = 9;

            switch (sLoai)
            {
                // sách
                case "0":
                    List<Sach> lstKQTK = book.SearchSach(sTuKhoa).ToList();
                    List<Sach> lstKQTKGY = book.GetTake3Sach().ToList();
                    if (lstKQTK.Count == 0)
                    {
                        ViewBag.ThongBao = "Không tìm thấy sản phẩm nào";
                        ViewBag.TB = "Sách mới !";
                        return View(lstKQTKGY.ToPagedList(pageNumber, pageSize));
                    }
                    ViewBag.ThongBao = "Đã tìm thấy " + lstKQTK.Count + " kết quả!";
                    return View(lstKQTK.ToPagedList(pageNumber, pageSize));
                //chủ đề
                case "1":
                    List<ClientWebsite.BookService.ChuDe> lstKQCD = book.SearchCD(sTuKhoa).ToList();
                    if(lstKQCD.Count ==0)
                    {
                        ViewBag.ThongBao = "Không tìm thấy kết quả nào !";
                        ViewBag.TB = "Chủ đề gợi ý !";
                        return View("Topics",book.getTake8ChuDe().ToList());
                    }
                    ViewBag.ThongBao = "Đã tìm thấy " + lstKQCD.Count + " kết quả!";
                    return View("Topics", lstKQCD);
                // tác giả
                case "2":
                    List<ClientWebsite.BookService.TacGia> lstKQTG = book.SearchTG(sTuKhoa).ToList();
                    if (lstKQTG.Count == 0)
                    {
                        ViewBag.ThongBao = "Không tìm thấy kết quả nào !";
                        ViewBag.TB = "Tác giả gợi ý !";
                        return View("Authors", book.getTake4TG().ToList());
                    }
                    ViewBag.ThongBao = "Đã tìm thấy " + lstKQTG.Count + " kết quả!";
                    return View("Authors", lstKQTG);
                // nxb
                case "3":
                    List<ClientWebsite.BookService.NhaXuatBan> lstKQNXB = book.SearchNXB(sTuKhoa).ToList();
                    if (lstKQNXB.Count == 0)
                    {
                        ViewBag.ThongBao = "Không tìm thấy kết quả nào !";
                        ViewBag.TB = "Nhà xuất bản gợi ý !";
                        return View("Publisher", book.getTake4NXB().ToList());
                    }
                    ViewBag.ThongBao = "Đã tìm thấy " + lstKQNXB.Count + " kết quả!";
                    return View("Publisher", lstKQNXB);
            }
            
            return View();
        }
        [HttpGet]
        public ActionResult SearchResults(int? page, string sTuKhoa)
        {
            ViewBag.TuKhoa = sTuKhoa;
            List<ClientWebsite.BookService.Sach> lstKQTK = book.SearchSach(sTuKhoa).ToList();
            List<ClientWebsite.BookService.Sach> lstKQTKGY = book.GetTake3Sach().ToList();
            //Phân trang
            int pageNumber = (page ?? 1);
            int pageSize = 9;
            if (lstKQTK.Count == 0)
            {
                ViewBag.ThongBao = "Không tìm thấy kết quả nào !";
                ViewBag.ThongBao = "Sách mới !";
                return View(lstKQTKGY.ToPagedList(pageNumber, pageSize));
            }
            ViewBag.ThongBao = "Đã tìm thấy " + lstKQTK.Count + " kết quả!";
            return View(lstKQTK.OrderBy(n => n.TenSach).ToPagedList(pageNumber, pageSize));
        }
    }
}