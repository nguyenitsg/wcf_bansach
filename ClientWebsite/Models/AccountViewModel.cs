﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebSiteBanSach.Models;

namespace WebSiteBanSach.Models
{
    public class AccountViewModel
    {
        public ClientWebsite.UserService.KhachHang ConverObjKhachHang(RegisterViewModel customer)
        {
            ClientWebsite.UserService.KhachHang kh = new ClientWebsite.UserService.KhachHang();
            kh.TaiKhoan = customer.Account;
            kh.MatKhau = customer.Password;
            return kh;
        }
        public ClientWebsite.UserService.KhachHang ConverObjKhachHangLogin(LoginViewModel login)
        {
            ClientWebsite.UserService.KhachHang kh = new ClientWebsite.UserService.KhachHang();
            kh.TaiKhoan = login.Account;
            kh.MatKhau = login.Password;
            return kh;
        }
    }
    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Account")]
        public string Account { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {

        [Required]
        [Display(Name = "Account")]
        public string Account { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}