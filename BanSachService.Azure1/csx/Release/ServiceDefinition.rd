﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="BanSachService.Azure1" generation="1" functional="0" release="0" Id="7fb406fb-7a5c-4d61-ba89-463b959009a3" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="BanSachService.Azure1Group" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="BanSachService:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/BanSachService.Azure1/BanSachService.Azure1Group/LB:BanSachService:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="BanSachService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/BanSachService.Azure1/BanSachService.Azure1Group/MapBanSachService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="BanSachServiceInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/BanSachService.Azure1/BanSachService.Azure1Group/MapBanSachServiceInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:BanSachService:Endpoint1">
          <toPorts>
            <inPortMoniker name="/BanSachService.Azure1/BanSachService.Azure1Group/BanSachService/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapBanSachService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/BanSachService.Azure1/BanSachService.Azure1Group/BanSachService/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapBanSachServiceInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/BanSachService.Azure1/BanSachService.Azure1Group/BanSachServiceInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="BanSachService" generation="1" functional="0" release="0" software="D:\WCF_Nhom3_BanSach\BanSachService.Azure1\csx\Release\roles\BanSachService" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;BanSachService&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;BanSachService&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/BanSachService.Azure1/BanSachService.Azure1Group/BanSachServiceInstances" />
            <sCSPolicyUpdateDomainMoniker name="/BanSachService.Azure1/BanSachService.Azure1Group/BanSachServiceUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/BanSachService.Azure1/BanSachService.Azure1Group/BanSachServiceFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="BanSachServiceUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="BanSachServiceFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="BanSachServiceInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="5bd47e32-3b23-4c19-8240-559fea3a3653" ref="Microsoft.RedDog.Contract\ServiceContract\BanSachService.Azure1Contract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="46203373-2256-4e19-8767-d91d5ed56a4a" ref="Microsoft.RedDog.Contract\Interface\BanSachService:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/BanSachService.Azure1/BanSachService.Azure1Group/BanSachService:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>