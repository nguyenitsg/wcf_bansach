﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="BanSachService.Azure" generation="1" functional="0" release="0" Id="84486e3e-8630-4948-8fbb-083f0a07e260" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="BanSachService.AzureGroup" generation="1" functional="0" release="0">
      <componentports>
        <inPort name="BanSachService:Endpoint1" protocol="http">
          <inToChannel>
            <lBChannelMoniker name="/BanSachService.Azure/BanSachService.AzureGroup/LB:BanSachService:Endpoint1" />
          </inToChannel>
        </inPort>
      </componentports>
      <settings>
        <aCS name="BanSachService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/BanSachService.Azure/BanSachService.AzureGroup/MapBanSachService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="BanSachServiceInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/BanSachService.Azure/BanSachService.AzureGroup/MapBanSachServiceInstances" />
          </maps>
        </aCS>
      </settings>
      <channels>
        <lBChannel name="LB:BanSachService:Endpoint1">
          <toPorts>
            <inPortMoniker name="/BanSachService.Azure/BanSachService.AzureGroup/BanSachService/Endpoint1" />
          </toPorts>
        </lBChannel>
      </channels>
      <maps>
        <map name="MapBanSachService:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/BanSachService.Azure/BanSachService.AzureGroup/BanSachService/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapBanSachServiceInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/BanSachService.Azure/BanSachService.AzureGroup/BanSachServiceInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="BanSachService" generation="1" functional="0" release="0" software="D:\WCF_NHOM3\BanSachService.Azure\csx\Release\roles\BanSachService" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaIISHost.exe " memIndex="-1" hostingEnvironment="frontendadmin" hostingEnvironmentVersion="2">
            <componentports>
              <inPort name="Endpoint1" protocol="http" portRanges="80" />
            </componentports>
            <settings>
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;BanSachService&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;BanSachService&quot;&gt;&lt;e name=&quot;Endpoint1&quot; /&gt;&lt;/r&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/BanSachService.Azure/BanSachService.AzureGroup/BanSachServiceInstances" />
            <sCSPolicyUpdateDomainMoniker name="/BanSachService.Azure/BanSachService.AzureGroup/BanSachServiceUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/BanSachService.Azure/BanSachService.AzureGroup/BanSachServiceFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="BanSachServiceUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="BanSachServiceFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="BanSachServiceInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
  <implements>
    <implementation Id="5d03f16d-6c36-4218-be7d-64f0dfa7167b" ref="Microsoft.RedDog.Contract\ServiceContract\BanSachService.AzureContract@ServiceDefinition">
      <interfacereferences>
        <interfaceReference Id="93363d30-7318-4774-95b4-08ac742b2ac8" ref="Microsoft.RedDog.Contract\Interface\BanSachService:Endpoint1@ServiceDefinition">
          <inPort>
            <inPortMoniker name="/BanSachService.Azure/BanSachService.AzureGroup/BanSachService:Endpoint1" />
          </inPort>
        </interfaceReference>
      </interfacereferences>
    </implementation>
  </implements>
</serviceModel>