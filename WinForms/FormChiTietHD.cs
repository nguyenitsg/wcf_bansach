﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinForms.ServiceDonHang;

namespace WinForms
{
    public partial class FormChiTietHD : Form
    {
        DonHangEntity donhang = new DonHangEntity();
        public FormChiTietHD(DonHangEntity dh)
        {
            this.donhang = dh;
            InitializeComponent();
        }

        private void FormChiTietHD_Load(object sender, EventArgs e)
        {
            txtMaDH.Text = donhang.MaKH.ToString();
            txtMaKH.Text = donhang.MaDonHang.ToString();
            txtMaSach.Text = donhang.MaSach.ToString();
            txtNgayDat.Text = donhang.NgayDat.ToString();
            txtNgayGiao.Text = donhang.NgayGiao.ToString();
            txt_SoLuong.Text = donhang.SoLuong.ToString();
            txtDonGia.Text = donhang.DonGia.ToString();


           
            if (donhang.DaThanhToan != null && donhang.DaThanhToan.ToString() != String.Empty)
            {
                txtThanhToan.Text = donhang.DaThanhToan.ToString();
            }
            if (donhang.TinhTrangGiaoHang != null && donhang.TinhTrangGiaoHang.ToString() != String.Empty)
            {
                txtTinhTrang.Text = donhang.TinhTrangGiaoHang.ToString();
            }

        }

        private void btn_Luu_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
