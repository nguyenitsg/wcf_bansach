﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinForms.ServiceDonHang;
using WinForms.ServiceSach;

namespace WinForms
{
    public partial class FormTimKiem : Form
    {
        SachServiceClient sach = new SachServiceClient();
        FormQuanLy _frm;
        int t = 1;
        public FormTimKiem(FormQuanLy _frm)
        {
            this._frm = _frm;
            InitializeComponent();
        }

        void clearDGV()
        {
            // remove selected rows 
            foreach (DataGridViewRow row in GridView_TimKiem.Rows)
            {
                GridView_TimKiem.Rows.Remove(row);
            }
        }
        private void btn_TimKiem_Click(object sender, EventArgs e)
        {

            
            GridView_TimKiem.DataSource = null;
            GridView_TimKiem.DataSource = sach.SearchSach(txtTimKiem.Text).ToList();
          
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            DataGridViewTextBoxColumn textboxColumn = new DataGridViewTextBoxColumn();
            GridView_TimKiem.Columns[0].Width = 120;
            GridView_TimKiem.Columns[0].HeaderCell.Value = "Mã Sách";
            GridView_TimKiem.Columns[0].DataPropertyName = "MaSach";

            GridView_TimKiem.Columns[1].Width = 120;
            GridView_TimKiem.Columns[1].HeaderCell.Value = "Mã NXB";
            GridView_TimKiem.Columns[1].DataPropertyName = "MaNXB";

            GridView_TimKiem.Columns[2].Width = 120;
            GridView_TimKiem.Columns[2].HeaderCell.Value = "Tên Sách";
            GridView_TimKiem.Columns[2].DataPropertyName = "TenSach";

            GridView_TimKiem.Columns[3].Width = 120;
            GridView_TimKiem.Columns[3].HeaderCell.Value = "Số Lượng";
            GridView_TimKiem.Columns[3].DataPropertyName = "SoLuongTon";

            GridView_TimKiem.Columns[4].Width = 120;
            GridView_TimKiem.Columns[4].HeaderCell.Value = "Giá Bán";
            GridView_TimKiem.Columns[4].DataPropertyName = "GiaBan";

          
           
            checkColumn.Name = "Check";
            textboxColumn.HeaderText = "Check";
            checkColumn.Width = 150;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10;

            textboxColumn.Name = "txtSoLuong";
            textboxColumn.HeaderText = "Số Lượng Mua";
            textboxColumn.Width = 200;
            textboxColumn.ReadOnly = false;
            textboxColumn.FillWeight = 10;

        
            GridView_TimKiem.Columns.Add(checkColumn);
            GridView_TimKiem.Columns.Add(textboxColumn);


            GridView_TimKiem.Columns["MaSach"].Visible = false;
            GridView_TimKiem.Columns["TenSach"].Visible = false;
            GridView_TimKiem.Columns["NgayCapNhat"].Visible = false;
            GridView_TimKiem.Columns["MoTa"].Visible = false;
            GridView_TimKiem.Columns["Moi"].Visible = false;
            GridView_TimKiem.Columns["NhaXuatBan"].Visible = false;
            GridView_TimKiem.Columns["SoLuongTon"].Visible = false;
            if(t!=1)
            {

                GridView_TimKiem.Columns["MaChuDe"].Visible = false;
                GridView_TimKiem.Columns["MaNXB"].Visible = false;
            }
            t = 2;
            
        }

        private void FormTimKiem_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTaoHD_Click(object sender, EventArgs e)
        {
            List<DonHangEntity> list = new List<DonHangEntity>();
            foreach (DataGridViewRow row in GridView_TimKiem.Rows)
            {
                if (Convert.ToBoolean(row.Cells[12].Value)==true)
                {
                   // MessageBox.Show(row.Cells[12].Value.ToString());
                    DonHangEntity dh = new DonHangEntity();
                    dh.MaKH = 5;
                    dh.MaSach = Int32.Parse(row.Cells[0].Value.ToString());
                    if (row.Cells[13].Value==null)
                    {
                        dh.SoLuong = 1;
                    }
                    else
                    {
                        dh.SoLuong = Int32.Parse(row.Cells[13].Value.ToString());
                    }
                   
                    dh.DonGia = Int32.Parse(row.Cells[4].Value.ToString());
                    list.Add(dh);
                }

                
            }
            FormThemDH themdh = new FormThemDH(list, _frm);
            themdh.Show();
        }

        private void GridView_TimKiem_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
