﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinForms.ServiceKhachHang;

namespace WinForms
{
    public partial class FormDangNhap : Form
    {
        NguoiDungServiceClient sv = new NguoiDungServiceClient();
        public FormDangNhap()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnDangNhap_Click(object sender, EventArgs e)
        {
            KhachHang kh = sv.getKhachHang(txtTaiKhoan.Text);
            if(kh!=null)
            {
                if (kh.TaiKhoan == txtTaiKhoan.Text && kh.MatKhau == txtMatKhau.Text && kh.roles == 1)
                {
                    this.Hide();
                    FormQuanLy frm = new FormQuanLy();
                    frm.Show();
                }
                else
                {
                    MessageBox.Show("Đăng nhập thất bại !");
                }
            }
            else
            {
                MessageBox.Show("Tài khoản không tồn tại !");
            }
           
        }
    }
}
