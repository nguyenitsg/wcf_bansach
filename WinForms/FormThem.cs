﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinForms.ServiceKhachHang;

namespace WinForms
{
    public partial class FormThem : Form
    {
        ServiceKhachHang.NguoiDungServiceClient sv = new ServiceKhachHang.NguoiDungServiceClient();
        KhachHang kh;
        FormQuanLy _frm;
        public FormThem(FormQuanLy _frm)
        {
            this._frm = _frm;
            InitializeComponent();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Bắt lổi sự kiện thêm thông tin khách hàng

            Boolean kiemtra = true;
            if (txtMatKhau.Text != txtNhapLaiMK.Text)
            {
                MessageBox.Show("Xin nhập lại mật khẩu");
                kiemtra = false;
            }
            if (txtTaiKhoan.Text == ""  || txtMatKhau.Text == "" || txtNhapLaiMK.Text == ""
               || txtHoTen.Text == "")
            {
                MessageBox.Show("Xin nhập đầy đủ thông tin !");
                kiemtra = false;
               

            }
            else
            {
               
                //Lấy thông tin từ form và cho vào đối tượng khách hàng
                kh = new KhachHang();
                kh.TaiKhoan = txtTaiKhoan.Text;
                kh.MatKhau = txtMatKhau.Text;
                kh.HoTen = txtHoTen.Text;
                /*
                kh.DiaChi = txtDiaChi.Text;
                kh.DienThoai = txtSoDT.Text;
                kh.NgaySinh = DateTime.Parse(dateTimePicker_NgaySinh.Value.ToString());
                kh.Email = txtEmail.Text;
                kh.roles = 0;
                if (radioButton_Nam.Checked == true)
                {
                    kh.GioiTinh = radioButton_Nam.Text;
                }
                if (radioButton_Nu.Checked == true)
                {
                    kh.GioiTinh = radioButton_Nu.Text;
                }
                */
                //Thêm đối tượng khách hàng vào csdl
                if (sv.AddKhachHang(kh) == true && kiemtra == true)
                {
                    MessageBox.Show("Thêm thành công !");
                    this.Close();
                    _frm.updateGrid();
                }
                else
                    MessageBox.Show("Thêm thất bại !");
            } 
        }

        private void button1_Click(object sender, EventArgs e)
        {          
              this.Close();
        }

        private void FormThem_Load(object sender, EventArgs e)
        {

        }
    }
}
