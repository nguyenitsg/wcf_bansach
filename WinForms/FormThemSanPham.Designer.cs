﻿namespace WinForms
{
    partial class FormThemSanPham
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_Huy = new System.Windows.Forms.Button();
            this.btn_Them = new System.Windows.Forms.Button();
            this.txtTenSach = new System.Windows.Forms.TextBox();
            this.txtGiaBan = new System.Windows.Forms.TextBox();
            this.txtSoLuongTon = new System.Windows.Forms.TextBox();
            this.comboBox_CD = new System.Windows.Forms.ComboBox();
            this.comboBox_Moi = new System.Windows.Forms.ComboBox();
            this.comboBox_NXB = new System.Windows.Forms.ComboBox();
            this.txt_AnhBia = new System.Windows.Forms.TextBox();
            this.btn_browse = new System.Windows.Forms.Button();
            this.dateTimePicker_NgayCapNhat = new System.Windows.Forms.DateTimePicker();
            this.richTextBox_MoTa = new System.Windows.Forms.RichTextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(482, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Thêm Mới Sản Phẩm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(54, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Tên Sách : ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(54, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Giá Bán : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(54, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ngày Cập Nhật : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(54, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(63, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Ảnh Bìa : ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(54, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Số Lượng Tồn : ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(54, 244);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Chủ Đề : ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(54, 276);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Nhà Xuất Bản : ";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(54, 307);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 16);
            this.label9.TabIndex = 8;
            this.label9.Text = "Mới : ";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(54, 367);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(56, 16);
            this.label10.TabIndex = 9;
            this.label10.Text = "Mô Tả : ";
            // 
            // btn_Huy
            // 
            this.btn_Huy.Location = new System.Drawing.Point(1052, 562);
            this.btn_Huy.Name = "btn_Huy";
            this.btn_Huy.Size = new System.Drawing.Size(101, 23);
            this.btn_Huy.TabIndex = 10;
            this.btn_Huy.Text = "Hủy";
            this.btn_Huy.UseVisualStyleBackColor = true;
            // 
            // btn_Them
            // 
            this.btn_Them.Location = new System.Drawing.Point(931, 562);
            this.btn_Them.Name = "btn_Them";
            this.btn_Them.Size = new System.Drawing.Size(105, 23);
            this.btn_Them.TabIndex = 11;
            this.btn_Them.Text = "Lưu";
            this.btn_Them.UseVisualStyleBackColor = true;
            this.btn_Them.Click += new System.EventHandler(this.btn_Them_Click);
            // 
            // txtTenSach
            // 
            this.txtTenSach.Location = new System.Drawing.Point(189, 84);
            this.txtTenSach.Name = "txtTenSach";
            this.txtTenSach.Size = new System.Drawing.Size(928, 20);
            this.txtTenSach.TabIndex = 12;
            // 
            // txtGiaBan
            // 
            this.txtGiaBan.Location = new System.Drawing.Point(189, 114);
            this.txtGiaBan.Name = "txtGiaBan";
            this.txtGiaBan.Size = new System.Drawing.Size(928, 20);
            this.txtGiaBan.TabIndex = 13;
            // 
            // txtSoLuongTon
            // 
            this.txtSoLuongTon.Location = new System.Drawing.Point(189, 213);
            this.txtSoLuongTon.Name = "txtSoLuongTon";
            this.txtSoLuongTon.Size = new System.Drawing.Size(928, 20);
            this.txtSoLuongTon.TabIndex = 14;
            // 
            // comboBox_CD
            // 
            this.comboBox_CD.FormattingEnabled = true;
            this.comboBox_CD.Location = new System.Drawing.Point(189, 239);
            this.comboBox_CD.Name = "comboBox_CD";
            this.comboBox_CD.Size = new System.Drawing.Size(928, 21);
            this.comboBox_CD.TabIndex = 15;
            // 
            // comboBox_Moi
            // 
            this.comboBox_Moi.FormattingEnabled = true;
            this.comboBox_Moi.Items.AddRange(new object[] {
            "0",
            "1"});
            this.comboBox_Moi.Location = new System.Drawing.Point(189, 307);
            this.comboBox_Moi.Name = "comboBox_Moi";
            this.comboBox_Moi.Size = new System.Drawing.Size(928, 21);
            this.comboBox_Moi.TabIndex = 16;
            // 
            // comboBox_NXB
            // 
            this.comboBox_NXB.FormattingEnabled = true;
            this.comboBox_NXB.Location = new System.Drawing.Point(189, 271);
            this.comboBox_NXB.Name = "comboBox_NXB";
            this.comboBox_NXB.Size = new System.Drawing.Size(928, 21);
            this.comboBox_NXB.TabIndex = 16;
            // 
            // txt_AnhBia
            // 
            this.txt_AnhBia.Location = new System.Drawing.Point(189, 173);
            this.txt_AnhBia.Name = "txt_AnhBia";
            this.txt_AnhBia.Size = new System.Drawing.Size(795, 20);
            this.txt_AnhBia.TabIndex = 17;
            // 
            // btn_browse
            // 
            this.btn_browse.Location = new System.Drawing.Point(1012, 170);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(105, 23);
            this.btn_browse.TabIndex = 18;
            this.btn_browse.Text = "Browse";
            this.btn_browse.UseVisualStyleBackColor = true;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // dateTimePicker_NgayCapNhat
            // 
            this.dateTimePicker_NgayCapNhat.Location = new System.Drawing.Point(189, 140);
            this.dateTimePicker_NgayCapNhat.Name = "dateTimePicker_NgayCapNhat";
            this.dateTimePicker_NgayCapNhat.Size = new System.Drawing.Size(928, 20);
            this.dateTimePicker_NgayCapNhat.TabIndex = 19;
            // 
            // richTextBox_MoTa
            // 
            this.richTextBox_MoTa.Location = new System.Drawing.Point(189, 367);
            this.richTextBox_MoTa.Name = "richTextBox_MoTa";
            this.richTextBox_MoTa.Size = new System.Drawing.Size(928, 175);
            this.richTextBox_MoTa.TabIndex = 20;
            this.richTextBox_MoTa.Text = "";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FormThemSanPham
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1165, 597);
            this.Controls.Add(this.richTextBox_MoTa);
            this.Controls.Add(this.dateTimePicker_NgayCapNhat);
            this.Controls.Add(this.btn_browse);
            this.Controls.Add(this.txt_AnhBia);
            this.Controls.Add(this.comboBox_NXB);
            this.Controls.Add(this.comboBox_Moi);
            this.Controls.Add(this.comboBox_CD);
            this.Controls.Add(this.txtSoLuongTon);
            this.Controls.Add(this.txtGiaBan);
            this.Controls.Add(this.txtTenSach);
            this.Controls.Add(this.btn_Them);
            this.Controls.Add(this.btn_Huy);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormThemSanPham";
            this.Text = "Thêm Sản Phẩm";
            this.Load += new System.EventHandler(this.FormThemSanPham_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_Huy;
        private System.Windows.Forms.Button btn_Them;
        private System.Windows.Forms.TextBox txtTenSach;
        private System.Windows.Forms.TextBox txtGiaBan;
        private System.Windows.Forms.TextBox txtSoLuongTon;
        private System.Windows.Forms.ComboBox comboBox_CD;
        private System.Windows.Forms.ComboBox comboBox_Moi;
        private System.Windows.Forms.ComboBox comboBox_NXB;
        private System.Windows.Forms.TextBox txt_AnhBia;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.DateTimePicker dateTimePicker_NgayCapNhat;
        private System.Windows.Forms.RichTextBox richTextBox_MoTa;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}