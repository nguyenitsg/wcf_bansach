﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinForms.ServiceSach;

namespace WinForms
{
    public partial class FormSuaSanPham : Form
    {
        SachServiceClient sach = new SachServiceClient();
        Sach_NXB_CDEntity sach_nxb_cd = new Sach_NXB_CDEntity();
        FormQuanLy _frm;
        public FormSuaSanPham(Sach_NXB_CDEntity sach_nxb_cd, FormQuanLy _frm)
        {
            this.sach_nxb_cd = sach_nxb_cd;
            this._frm = _frm;
            InitializeComponent();
        }

        private void FormSuaSanPham_Load(object sender, EventArgs e)
        {
            //tạo list tên chủ đề
            List<string> list_CD = new List<string>();
            foreach (var item in sach.GetAllCD())
            {
                list_CD.Add(item.TenChuDe);
            }
            // tạo list tên nhà xuất bản

            List<string> list_NXB = new List<string>();
            foreach (var item in sach.GetAllNXB())
            {
                list_NXB.Add(item.TenNXB);
            }

            comboBox_CD.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_NXB.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_Moi.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_CD.DataSource = list_CD;
            comboBox_NXB.DataSource = list_NXB;

            //load thông tin sủa sản phẩm
            txtTenSach.Text = sach_nxb_cd.TenSach;
            txtGiaBan.Text = sach_nxb_cd.GiaBan.ToString();
            txtSoLuongTon.Text = sach_nxb_cd.SoLuongTon.ToString();
            txt_AnhBia.Text = sach_nxb_cd.AnhBia;
            richTextBox_MoTa.Text = sach_nxb_cd.MoTa;
            dateTimePicker_NgayCapNhat.Value = DateTime.Parse(sach_nxb_cd.NgayCapNhat.ToString());
            comboBox_CD.Text = sach_nxb_cd.TenCD;
            comboBox_NXB.Text = sach_nxb_cd.TenNXB;
            comboBox_Moi.Text = sach_nxb_cd.Moi.ToString();

        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            int size = -1;
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    string text = File.ReadAllText(file);
                    txt_AnhBia.Text = openFileDialog1.SafeFileName;
                    size = text.Length;
                }
                catch (IOException)
                {
                }
            }
        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            NhaXuatBan nxb = sach.MaNXB(comboBox_NXB.Text);
            ChuDe cd = sach.MaCD(comboBox_CD.Text);
            //Tạo đối tượng sách để update csdl
            Sach sach_update = new Sach();
            sach_update.MaSach = sach_nxb_cd.MaSach;
            sach_update.TenSach = txtTenSach.Text;
            sach_update.GiaBan = Int32.Parse(txtGiaBan.Text);
            sach_update.AnhBia = txt_AnhBia.Text;
            sach_update.MoTa = richTextBox_MoTa.Text;
            sach_update.Moi = Int32.Parse(comboBox_Moi.Text);
            sach_update.NgayCapNhat = dateTimePicker_NgayCapNhat.Value;
            sach_update.MaNXB = nxb.MaNXB;
            sach_update.MaChuDe = cd.MaChuDe;
            sach_update.SoLuongTon = Int32.Parse(txtSoLuongTon.Text);

            if(sach.UpdateSach(sach_update))
            {
                MessageBox.Show("Update thành công !");
                this.Close();
                _frm.updateGrid();
            }
            else
            {
                MessageBox.Show("Update thất bại !");
            }
          //  MessageBox.Show(sach_update.MaSach.ToString() +"///"+ sach_update.TenNXB.ToString() +"//" + sach_update.MoTa.ToString());



            //lấy vị trí file
            string fileLocation = openFileDialog1.FileName;
            if (fileLocation != "openFileDialog1") // Test result.
            {
                //lấy đường dẫn lưu
                var path = Path.GetFullPath("../../../ClientWebsite/HinhAnhSP");
                //kiểm tra ảnh tồn tại chưa
                //copy file ảnh vào thư mục
                File.Copy(fileLocation, Path.Combine(path, Path.GetFileName(fileLocation)), true);
            }
        }
    }
}
