﻿namespace WinForms
{
    partial class FormQuanLy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabKhachHang = new System.Windows.Forms.TabPage();
            this.btn_TimKiem = new System.Windows.Forms.Button();
            this.txt_TimKiem = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.GridView_KhachHang = new System.Windows.Forms.DataGridView();
            this.btnThem = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabDonHang = new System.Windows.Forms.TabPage();
            this.btn_TimKiem_DH = new System.Windows.Forms.Button();
            this.txt_TimKiem_MaDH = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_taodh = new System.Windows.Forms.Button();
            this.btn_HuyDH = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridView_DonHang = new System.Windows.Forms.DataGridView();
            this.tabPage_SanPham = new System.Windows.Forms.TabPage();
            this.btn_TimKiem_TenSach = new System.Windows.Forms.Button();
            this.txt_TimKiem_TenSach = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_Huy = new System.Windows.Forms.Button();
            this.btn_ThemSanPham = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.dataGridView_Sach = new System.Windows.Forms.DataGridView();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.tabControl1.SuspendLayout();
            this.tabKhachHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_KhachHang)).BeginInit();
            this.tabDonHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_DonHang)).BeginInit();
            this.tabPage_SanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Sach)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabKhachHang);
            this.tabControl1.Controls.Add(this.tabDonHang);
            this.tabControl1.Controls.Add(this.tabPage_SanPham);
            this.tabControl1.Location = new System.Drawing.Point(-3, -2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(50, 5);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1204, 523);
            this.tabControl1.TabIndex = 0;
            // 
            // tabKhachHang
            // 
            this.tabKhachHang.Controls.Add(this.btn_TimKiem);
            this.tabKhachHang.Controls.Add(this.txt_TimKiem);
            this.tabKhachHang.Controls.Add(this.label4);
            this.tabKhachHang.Controls.Add(this.GridView_KhachHang);
            this.tabKhachHang.Controls.Add(this.btnThem);
            this.tabKhachHang.Controls.Add(this.label1);
            this.tabKhachHang.Location = new System.Drawing.Point(4, 26);
            this.tabKhachHang.Name = "tabKhachHang";
            this.tabKhachHang.Padding = new System.Windows.Forms.Padding(3);
            this.tabKhachHang.Size = new System.Drawing.Size(1196, 493);
            this.tabKhachHang.TabIndex = 0;
            this.tabKhachHang.Text = "Khách Hàng";
            this.tabKhachHang.UseVisualStyleBackColor = true;
            this.tabKhachHang.Click += new System.EventHandler(this.tabKhachHang_Click);
            // 
            // btn_TimKiem
            // 
            this.btn_TimKiem.Location = new System.Drawing.Point(542, 49);
            this.btn_TimKiem.Name = "btn_TimKiem";
            this.btn_TimKiem.Size = new System.Drawing.Size(118, 24);
            this.btn_TimKiem.TabIndex = 7;
            this.btn_TimKiem.Text = "Tìm Kiếm";
            this.btn_TimKiem.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btn_TimKiem.UseVisualStyleBackColor = true;
            this.btn_TimKiem.Click += new System.EventHandler(this.btn_TimKiem_Click);
            // 
            // txt_TimKiem
            // 
            this.txt_TimKiem.Location = new System.Drawing.Point(274, 53);
            this.txt_TimKiem.Name = "txt_TimKiem";
            this.txt_TimKiem.Size = new System.Drawing.Size(247, 20);
            this.txt_TimKiem.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(180, 54);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 15);
            this.label4.TabIndex = 4;
            this.label4.Text = "Nhập Tên TK : ";
            // 
            // GridView_KhachHang
            // 
            this.GridView_KhachHang.AllowUserToAddRows = false;
            this.GridView_KhachHang.AllowUserToDeleteRows = false;
            this.GridView_KhachHang.AllowUserToOrderColumns = true;
            this.GridView_KhachHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.GridView_KhachHang.Location = new System.Drawing.Point(26, 81);
            this.GridView_KhachHang.Name = "GridView_KhachHang";
            this.GridView_KhachHang.ReadOnly = true;
            this.GridView_KhachHang.Size = new System.Drawing.Size(1136, 345);
            this.GridView_KhachHang.TabIndex = 2;
            this.GridView_KhachHang.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.GridView_KhachHang_CellContentDoubleClick);
            // 
            // btnThem
            // 
            this.btnThem.Location = new System.Drawing.Point(26, 50);
            this.btnThem.Name = "btnThem";
            this.btnThem.Size = new System.Drawing.Size(118, 24);
            this.btnThem.TabIndex = 1;
            this.btnThem.Text = "Thêm ";
            this.btnThem.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage;
            this.btnThem.UseVisualStyleBackColor = true;
            this.btnThem.Click += new System.EventHandler(this.btnThem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(319, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(235, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Quan Ly Khach Hang";
            // 
            // tabDonHang
            // 
            this.tabDonHang.Controls.Add(this.btn_TimKiem_DH);
            this.tabDonHang.Controls.Add(this.txt_TimKiem_MaDH);
            this.tabDonHang.Controls.Add(this.label5);
            this.tabDonHang.Controls.Add(this.btn_taodh);
            this.tabDonHang.Controls.Add(this.btn_HuyDH);
            this.tabDonHang.Controls.Add(this.label2);
            this.tabDonHang.Controls.Add(this.dataGridView_DonHang);
            this.tabDonHang.Location = new System.Drawing.Point(4, 26);
            this.tabDonHang.Name = "tabDonHang";
            this.tabDonHang.Padding = new System.Windows.Forms.Padding(3);
            this.tabDonHang.Size = new System.Drawing.Size(1196, 493);
            this.tabDonHang.TabIndex = 1;
            this.tabDonHang.Text = "Đơn Hàng";
            this.tabDonHang.UseVisualStyleBackColor = true;
            // 
            // btn_TimKiem_DH
            // 
            this.btn_TimKiem_DH.Location = new System.Drawing.Point(519, 63);
            this.btn_TimKiem_DH.Name = "btn_TimKiem_DH";
            this.btn_TimKiem_DH.Size = new System.Drawing.Size(75, 22);
            this.btn_TimKiem_DH.TabIndex = 6;
            this.btn_TimKiem_DH.Text = "Tìm Kiếm";
            this.btn_TimKiem_DH.UseVisualStyleBackColor = true;
            this.btn_TimKiem_DH.Click += new System.EventHandler(this.btn_TimKiem_DH_Click);
            // 
            // txt_TimKiem_MaDH
            // 
            this.txt_TimKiem_MaDH.Location = new System.Drawing.Point(233, 65);
            this.txt_TimKiem_MaDH.Name = "txt_TimKiem_MaDH";
            this.txt_TimKiem_MaDH.Size = new System.Drawing.Size(259, 20);
            this.txt_TimKiem_MaDH.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(139, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Nhập Mã ĐH : ";
            // 
            // btn_taodh
            // 
            this.btn_taodh.Location = new System.Drawing.Point(11, 58);
            this.btn_taodh.Name = "btn_taodh";
            this.btn_taodh.Size = new System.Drawing.Size(75, 22);
            this.btn_taodh.TabIndex = 3;
            this.btn_taodh.Text = "Tạo ĐH";
            this.btn_taodh.UseVisualStyleBackColor = true;
            this.btn_taodh.Click += new System.EventHandler(this.btn_taodh_Click);
            // 
            // btn_HuyDH
            // 
            this.btn_HuyDH.Location = new System.Drawing.Point(1094, 454);
            this.btn_HuyDH.Name = "btn_HuyDH";
            this.btn_HuyDH.Size = new System.Drawing.Size(75, 23);
            this.btn_HuyDH.TabIndex = 2;
            this.btn_HuyDH.Text = "Hủy";
            this.btn_HuyDH.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(524, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(188, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Quản Lý Đơn Hàng";
            // 
            // dataGridView_DonHang
            // 
            this.dataGridView_DonHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_DonHang.Location = new System.Drawing.Point(11, 98);
            this.dataGridView_DonHang.Name = "dataGridView_DonHang";
            this.dataGridView_DonHang.Size = new System.Drawing.Size(1158, 340);
            this.dataGridView_DonHang.TabIndex = 0;
            this.dataGridView_DonHang.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_DonHang_CellContentDoubleClick);
            // 
            // tabPage_SanPham
            // 
            this.tabPage_SanPham.Controls.Add(this.btn_TimKiem_TenSach);
            this.tabPage_SanPham.Controls.Add(this.txt_TimKiem_TenSach);
            this.tabPage_SanPham.Controls.Add(this.label6);
            this.tabPage_SanPham.Controls.Add(this.btn_Huy);
            this.tabPage_SanPham.Controls.Add(this.btn_ThemSanPham);
            this.tabPage_SanPham.Controls.Add(this.label3);
            this.tabPage_SanPham.Controls.Add(this.dataGridView_Sach);
            this.tabPage_SanPham.Location = new System.Drawing.Point(4, 26);
            this.tabPage_SanPham.Name = "tabPage_SanPham";
            this.tabPage_SanPham.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage_SanPham.Size = new System.Drawing.Size(1196, 493);
            this.tabPage_SanPham.TabIndex = 2;
            this.tabPage_SanPham.Text = "Sản Phẩm";
            this.tabPage_SanPham.UseVisualStyleBackColor = true;
            // 
            // btn_TimKiem_TenSach
            // 
            this.btn_TimKiem_TenSach.Location = new System.Drawing.Point(593, 55);
            this.btn_TimKiem_TenSach.Name = "btn_TimKiem_TenSach";
            this.btn_TimKiem_TenSach.Size = new System.Drawing.Size(91, 25);
            this.btn_TimKiem_TenSach.TabIndex = 6;
            this.btn_TimKiem_TenSach.Text = "Tìm Kiếm";
            this.btn_TimKiem_TenSach.UseVisualStyleBackColor = true;
            this.btn_TimKiem_TenSach.Click += new System.EventHandler(this.btn_TimKiem_TenSach_Click);
            // 
            // txt_TimKiem_TenSach
            // 
            this.txt_TimKiem_TenSach.Location = new System.Drawing.Point(276, 60);
            this.txt_TimKiem_TenSach.Name = "txt_TimKiem_TenSach";
            this.txt_TimKiem_TenSach.Size = new System.Drawing.Size(280, 20);
            this.txt_TimKiem_TenSach.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(178, 59);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 15);
            this.label6.TabIndex = 4;
            this.label6.Text = "Nhập Tên Sách : ";
            // 
            // btn_Huy
            // 
            this.btn_Huy.Location = new System.Drawing.Point(1083, 449);
            this.btn_Huy.Name = "btn_Huy";
            this.btn_Huy.Size = new System.Drawing.Size(75, 23);
            this.btn_Huy.TabIndex = 3;
            this.btn_Huy.Text = "Hủy";
            this.btn_Huy.UseVisualStyleBackColor = true;
            // 
            // btn_ThemSanPham
            // 
            this.btn_ThemSanPham.Location = new System.Drawing.Point(43, 55);
            this.btn_ThemSanPham.Name = "btn_ThemSanPham";
            this.btn_ThemSanPham.Size = new System.Drawing.Size(91, 25);
            this.btn_ThemSanPham.TabIndex = 2;
            this.btn_ThemSanPham.Text = "Thêm ";
            this.btn_ThemSanPham.UseVisualStyleBackColor = true;
            this.btn_ThemSanPham.Click += new System.EventHandler(this.btn_ThemSanPham_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(470, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(214, 25);
            this.label3.TabIndex = 1;
            this.label3.Text = "Quản Lý Sản Phẩm";
            // 
            // dataGridView_Sach
            // 
            this.dataGridView_Sach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Sach.Location = new System.Drawing.Point(43, 97);
            this.dataGridView_Sach.Name = "dataGridView_Sach";
            this.dataGridView_Sach.Size = new System.Drawing.Size(1115, 333);
            this.dataGridView_Sach.TabIndex = 0;
            this.dataGridView_Sach.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_Sach_CellContentClick);
            this.dataGridView_Sach.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dataGridView_Sach_MouseClick_1);
            // 
            // FormQuanLy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1199, 533);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormQuanLy";
            this.Text = "Quan Ly";
            this.Load += new System.EventHandler(this.FormQuanLy_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabKhachHang.ResumeLayout(false);
            this.tabKhachHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridView_KhachHang)).EndInit();
            this.tabDonHang.ResumeLayout(false);
            this.tabDonHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_DonHang)).EndInit();
            this.tabPage_SanPham.ResumeLayout(false);
            this.tabPage_SanPham.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Sach)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabKhachHang;
        private System.Windows.Forms.DataGridView GridView_KhachHang;
        private System.Windows.Forms.Button btnThem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabDonHang;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridView_DonHang;
        private System.Windows.Forms.Button btn_HuyDH;
        private System.Windows.Forms.TabPage tabPage_SanPham;
        private System.Windows.Forms.Button btn_Huy;
        private System.Windows.Forms.Button btn_ThemSanPham;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView_Sach;
        private System.Windows.Forms.Button btn_taodh;
        private System.Windows.Forms.Button btn_TimKiem;
        private System.Windows.Forms.TextBox txt_TimKiem;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_TimKiem_DH;
        private System.Windows.Forms.TextBox txt_TimKiem_MaDH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_TimKiem_TenSach;
        private System.Windows.Forms.TextBox txt_TimKiem_TenSach;
        private System.Windows.Forms.Label label6;
    }
}