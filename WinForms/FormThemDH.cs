﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinForms.ServiceDonHang;
using WinForms.ServiceKhachHang;



namespace WinForms
{
    public partial class FormThemDH : Form
    {
        NguoiDungServiceClient nguoidung = new NguoiDungServiceClient();
        ServiceDonHangClient donhang = new ServiceDonHangClient();
        FormQuanLy _frm;
        List<DonHangEntity> listdh = new List<DonHangEntity>();
      
        public FormThemDH(List<DonHangEntity> dh, FormQuanLy _frm)
        {
            this._frm = _frm;
            this.listdh = dh;
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void FormThemDH_Load(object sender, EventArgs e)
        {
            dataGridView_Them.DataSource= listdh;

            dataGridView_Them.Columns[0].Width = 250;
            dataGridView_Them.Columns[0].HeaderCell.Value = "Mã Sách";
            dataGridView_Them.Columns[0].DataPropertyName = "MaSach";

            dataGridView_Them.Columns[1].Width = 250;
            dataGridView_Them.Columns[1].HeaderCell.Value = "Số Lượng";
            dataGridView_Them.Columns[1].DataPropertyName = "SoLuong";

            dataGridView_Them.Columns[2].Width = 250;
            dataGridView_Them.Columns[2].HeaderCell.Value = "Đơn Giá";
            dataGridView_Them.Columns[2].DataPropertyName = "DonGia";

            dataGridView_Them.Columns["MaKH"].Visible = false;
            dataGridView_Them.Columns["NgayDat"].Visible = false;
            dataGridView_Them.Columns["NgayGiao"].Visible = false;
            dataGridView_Them.Columns["TinhTrangGiaoHang"].Visible = false;
            dataGridView_Them.Columns["MaSach"].Visible = false;
            dataGridView_Them.Columns["SoLuong"].Visible = false;
            
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            WinForms.ServiceKhachHang.KhachHang khachhang = new WinForms.ServiceKhachHang.KhachHang();
            khachhang.HoTen = txtHoTen.Text;
            khachhang.DiaChi = txtDiaChi.Text;
            khachhang.DienThoai = txtSoDT.Text;
            khachhang.roles = 0;
            //MessageBox.Show(nguoidung.AddKhachHangID(khachhang).ToString());

            int makh = Int32.Parse(nguoidung.AddKhachHangID(khachhang).ToString());
            if (makh != 0)
            {
                WinForms.ServiceDonHang.DonHang dh = new WinForms.ServiceDonHang.DonHang();
                dh.MaKH = makh;
                dh.NgayDat = DateTime.Now;
                dh.NgayGiao = DateTime.Now.AddDays(2);
                int madh = Int32.Parse(donhang.AddDonHang(dh).ToString());
                if (madh!=0)
                {

                    bool ret = false;
                    foreach (var item in listdh)
                    {
                        WinForms.ServiceDonHang.ChiTietDonHang ctDH = new WinForms.ServiceDonHang.ChiTietDonHang();
                        ctDH.MaDonHang = madh;
                        ctDH.MaSach = item.MaSach;
                        ctDH.SoLuong = item.SoLuong;
                        ctDH.DonGia = (decimal)item.DonGia;
                        if (donhang.AddCTDonHang(ctDH) == true)
                        {
                            ret = true;
                        }
                        else
                        {
                            ret = false;
                        }

                    }
                    if(ret==true)
                    {
                        MessageBox.Show("Thêm thành công !");
                        this.Close();
                        _frm.updateGrid();
                    }
                    else
                    {
                        MessageBox.Show("Thêm thất bại !");

                    }
                }
                else
                {
                    MessageBox.Show("Thêm thất bại !");

                }
            }
            else
            {
                MessageBox.Show("Thêm thất bại !");
            }
             
           

        }
    }
}
