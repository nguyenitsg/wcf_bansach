﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinForms.ServiceKhachHang;
namespace WinForms
{
   
    public partial class FormSua : Form
    {
        int MaKH;
        string Hoten;
        string TaiKhoan;
        string MatKhau;
        string DiaChi;
        string DienThoai;
        string Email;
        int role;
        DateTime NgaySinh;
        string GioiTinh;
        KhachHang khachhang;
        KhachHang khachhang2;
        FormQuanLy _frm;
        NguoiDungServiceClient sv = new NguoiDungServiceClient();
        public FormSua(KhachHang  kh, FormQuanLy frm)
        {
            khachhang2 = kh;
            this.MaKH = kh.MaKH;
            this.Hoten = kh.HoTen;
            this.TaiKhoan = kh.TaiKhoan;
            this.MatKhau = kh.MatKhau;
            this.DiaChi = kh.DiaChi;
            this.DienThoai = kh.DienThoai;
            this.Email = kh.Email;
            if(kh.NgaySinh!=null && kh.NgaySinh.ToString()!=String.Empty)
            {
                this.NgaySinh = DateTime.Parse(kh.NgaySinh.ToString());
            }
            
            this.GioiTinh = kh.GioiTinh;
            this.role = kh.roles;
            this._frm = frm;
            InitializeComponent();
        }

        private void FormSua_Load(object sender, EventArgs e)
        {
            txtMaKH.Text = MaKH.ToString();
            txtTaiKhoan.Text = TaiKhoan;
            txtHoTen.Text = Hoten;
            txtMatKhau.Text = MatKhau;
            txtSoDT.Text = DienThoai;
            txtEmail.Text = Email;
            txtDiaChi.Text = DiaChi;
            if(khachhang2.NgaySinh != null && khachhang2.NgaySinh.ToString() != String.Empty)
            {
               
                dateTimePicker_NgaySinh.Value = NgaySinh;
            }
            else
            {
                dateTimePicker_NgaySinh.Value = DateTime.Now;
            }
            
            comboBox_Role.SelectedIndex = comboBox_Role.Items.IndexOf(role.ToString());
           if(GioiTinh!=null)
            {
                if (GioiTinh == "Nam")
                {
                    radioButton_Nam.Checked = true;
                    radioButton_Nu.Checked = false;
                }
                if (GioiTinh.ToString() == "Nữ")
                {
                    radioButton_Nu.Checked = true;
                    radioButton_Nam.Checked = false;
                }
            }


        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            khachhang = new KhachHang();
            khachhang.MaKH = Int32.Parse(txtMaKH.Text);
            khachhang.TaiKhoan = txtTaiKhoan.Text;
            khachhang.MatKhau = txtMatKhau.Text;
            khachhang.HoTen = txtHoTen.Text;
            khachhang.DiaChi = txtDiaChi.Text;
            khachhang.DienThoai = txtSoDT.Text;
            khachhang.NgaySinh = DateTime.Parse(dateTimePicker_NgaySinh.Value.ToString());
            khachhang.Email = txtEmail.Text;
            khachhang.roles = Int32.Parse(comboBox_Role.Text);
            if (radioButton_Nam.Checked == true)
            {
                khachhang.GioiTinh = radioButton_Nam.Text;
            }
            if (radioButton_Nu.Checked == true)
            {
                khachhang.GioiTinh = radioButton_Nu.Text;
            }
            if (sv.EditKhachHang(khachhang) == true)
            {
                MessageBox.Show("Lưu thành công !");
                
                this.Close();
                _frm.updateGrid() ;

            }
            else
            {
                MessageBox.Show("Lưu thất bại !");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
