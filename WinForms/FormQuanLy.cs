﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinForms.ServiceKhachHang;
using WinForms.ServiceDonHang;
using WinForms.ServiceSach;

namespace WinForms
{
    public partial class FormQuanLy : Form
    {
        NguoiDungServiceClient sv = new NguoiDungServiceClient();
        ServiceDonHangClient dh = new ServiceDonHangClient();
        SachServiceClient sach = new SachServiceClient();
        
        public FormQuanLy()
        {
            InitializeComponent();
            GridView_KhachHang.MouseClick += GridView_KhachHang_MouseClick;
            dataGridView_DonHang.MouseClick += DataGridView_DonHang_MouseClick;
        }

        private void DataGridView_DonHang_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenuStrip menu = new ContextMenuStrip();
                int position_now = GridView_KhachHang.HitTest(e.X, e.Y).RowIndex;
                if (position_now >= 0)
                {

                    menu.Items.Add("Xóa").Name = "Xóa";
                }
                menu.Show(GridView_KhachHang, new Point(e.X, e.Y));
                menu.ItemClicked += Menu_ItemClicked1; ;

            }
        }

        private void Menu_ItemClicked1(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name.ToString())
            {
                case "Sữa":

                    break;
                case "Xóa":
                   // MessageBox.Show(dataGridView_DonHang.CurrentRow.Cells[2].Value.ToString());
                    /*
                    if (dh.RemoveDH(Int32.Parse(dataGridView_DonHang.CurrentRow.Cells[2].Value.ToString())))
                    {
                        MessageBox.Show("Xóa Thành Công !");
                        updateGrid();
                    }
                    else
                    {
                        MessageBox.Show("Xóa Thất Bại !");
                    }
                   */
                    break;
            }
        }
      

        private void GridView_KhachHang_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                    ContextMenuStrip menu = new ContextMenuStrip();
                    int position_now = GridView_KhachHang.HitTest(e.X, e.Y).RowIndex;
                    if (position_now >= 0)
                    {
                 
                    menu.Items.Add("Xóa").Name = "Xóa";
                    }
                    menu.Show(GridView_KhachHang, new Point(e.X, e.Y));
                menu.ItemClicked += Menu_ItemClicked;

            }

        }

        private void Menu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch(e.ClickedItem.Name.ToString())
            {
                case "Sữa":
                    
                    break;
                case "Xóa":
                   if (sv.RemoveKhachHang(Int32.Parse(GridView_KhachHang.CurrentRow.Cells[0].Value.ToString())))
                    {
                        MessageBox.Show("Xóa Thành Công !");
                        updateGrid();
                    }
                    else
                    {
                        MessageBox.Show("Xóa Thất Bại !");
                    }
                    break;
            }
        }

        public void updateGrid()
        {
            GridView_KhachHang.DataSource = null;
            dataGridView_Sach.DataSource = null;
            dataGridView_DonHang.DataSource = null;
            GridView_KhachHang.DataSource = sv.getListKhachHang().ToList();
            dataGridView_DonHang.DataSource = dh.getAllDonHang().ToList();
            dataGridView_Sach.DataSource = sach.GetAllSach();
            CustomGrid();

        }
        
        private void FormQuanLy_Load(object sender, EventArgs e)
        {
            
            GridView_KhachHang.DataSource = sv.getListKhachHang().ToList();
            dataGridView_DonHang.DataSource = dh.getAllDonHang().ToList();
            dataGridView_Sach.DataSource = sach.GetAllSach();
            CustomGrid();
        }
        private void tabKhachHang_Click(object sender, EventArgs e)
        {

        }
       
        private void GridView_KhachHang_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            WinForms.ServiceKhachHang.KhachHang kh = new WinForms.ServiceKhachHang.KhachHang();
            kh.MaKH = Int32.Parse(GridView_KhachHang.CurrentRow.Cells[0].Value.ToString());
            kh.HoTen = GridView_KhachHang.CurrentRow.Cells[3].Value.ToString();
            
            
            if (GridView_KhachHang.CurrentRow.Cells[1].Value != null && GridView_KhachHang.CurrentRow.Cells[1].Value.ToString() != String.Empty)
            {
                kh.TaiKhoan = GridView_KhachHang.CurrentRow.Cells[1].Value.ToString();
            }
            if (GridView_KhachHang.CurrentRow.Cells[2].Value != null && GridView_KhachHang.CurrentRow.Cells[2].Value.ToString() != String.Empty)
            {
                kh.MatKhau = GridView_KhachHang.CurrentRow.Cells[2].Value.ToString();
            }
            // khong bat buoc nhap

            if (GridView_KhachHang.CurrentRow.Cells[5].Value != null && GridView_KhachHang.CurrentRow.Cells[5].Value.ToString()!=String.Empty)
            {
                kh.NgaySinh = DateTime.Parse(GridView_KhachHang.CurrentRow.Cells[5].Value.ToString());
            }
            if (GridView_KhachHang.CurrentRow.Cells[6].Value != null && GridView_KhachHang.CurrentRow.Cells[6].Value.ToString() != String.Empty)
            {
                kh.DiaChi = GridView_KhachHang.CurrentRow.Cells[6].Value.ToString();
            }
            if (GridView_KhachHang.CurrentRow.Cells[8].Value != null && GridView_KhachHang.CurrentRow.Cells[8].Value.ToString() != String.Empty)
            {
                kh.DienThoai = GridView_KhachHang.CurrentRow.Cells[8].Value.ToString();
            }
            if (GridView_KhachHang.CurrentRow.Cells[7].Value != null && GridView_KhachHang.CurrentRow.Cells[7].Value.ToString() != String.Empty)
            {
                kh.Email = GridView_KhachHang.CurrentRow.Cells[7].Value.ToString();
            }
            if (GridView_KhachHang.CurrentRow.Cells[4].Value != null && GridView_KhachHang.CurrentRow.Cells[4].Value.ToString() != String.Empty)
            {
                kh.GioiTinh = GridView_KhachHang.CurrentRow.Cells[4].Value.ToString();
            }
            if (GridView_KhachHang.CurrentRow.Cells["roles"].Value != null && GridView_KhachHang.CurrentRow.Cells["roles"].Value.ToString() != String.Empty)
            {
                kh.roles = Int32.Parse(GridView_KhachHang.CurrentRow.Cells["roles"].Value.ToString());
            }

            FormSua frm = new FormSua(kh, this);
            frm.Show();
            //MessageBox.Show(GridView_KhachHang.CurrentRow.Cells[8].Value.ToString());





        }

        private void btnThem_Click(object sender, EventArgs e)
        {
             FormThem form = new FormThem(this);
             form.Show();
        }

        private void dataGridView_DonHang_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DonHangEntity dh = new DonHangEntity();
            // dh.MaKH= Int32.Parse(dataGridView_DonHang.CurrentRow.Cells[0].Value.ToString());
            //1 don gia 2 ma dh 3 ma kh 4 ma sach 5 ngay dat 6 ngay giao  7 so luong 0 /Da thanh toan 8/ Tinh trang
           
            dh.MaKH = Int32.Parse(dataGridView_DonHang.CurrentRow.Cells["MaKH"].Value.ToString());
            dh.MaDonHang= Int32.Parse(dataGridView_DonHang.CurrentRow.Cells["MaDonHang"].Value.ToString());
            dh.MaSach = Int32.Parse(dataGridView_DonHang.CurrentRow.Cells["MaSach"].Value.ToString());
            dh.NgayDat = DateTime.Parse(dataGridView_DonHang.CurrentRow.Cells["NgayDat"].Value.ToString());
            dh.NgayGiao = DateTime.Parse(dataGridView_DonHang.CurrentRow.Cells["NgayGiao"].Value.ToString());
            dh.SoLuong = Int32.Parse(dataGridView_DonHang.CurrentRow.Cells["SoLuong"].Value.ToString());
            dh.DonGia = Int32.Parse(dataGridView_DonHang.CurrentRow.Cells["DonGia"].Value.ToString());
            if (dataGridView_DonHang.CurrentRow.Cells["DaThanhToan"].Value != null && dataGridView_DonHang.CurrentRow.Cells["DaThanhToan"].Value.ToString()!= String.Empty)
            {
                dh.DaThanhToan = dataGridView_DonHang.CurrentRow.Cells["DaThanhToan"].Value.ToString();
            }
            if (dataGridView_DonHang.CurrentRow.Cells["TinhTrangGiaoHang"].Value != null && dataGridView_DonHang.CurrentRow.Cells["TinhTrangGiaoHang"].Value.ToString()!=String.Empty)
            {
                dh.TinhTrangGiaoHang = Int32.Parse(dataGridView_DonHang.CurrentRow.Cells["TinhTrangGiaoHang"].Value.ToString());
            }
            FormChiTietHD ct = new FormChiTietHD(dh);
            ct.Show();
        }
       


        private void dataGridView_Sach_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Sach_NXB_CDEntity sach = new Sach_NXB_CDEntity();
            // cột 0 ảnh bìa, 1 giá bán, 2 mã sách, 3 mô tả, 4 mới, 5 ngày cập nhật, 6 số lượng tồn
            // 7 tên chủ đề, 8 nhà xuất bản, 9 tên sách
            sach.MaSach = Int32.Parse(dataGridView_Sach.CurrentRow.Cells["MaSach"].Value.ToString());
            sach.TenSach = dataGridView_Sach.CurrentRow.Cells["TenSach"].Value.ToString();
            sach.GiaBan = Int32.Parse(dataGridView_Sach.CurrentRow.Cells["GiaBan"].Value.ToString());
            sach.AnhBia = dataGridView_Sach.CurrentRow.Cells["AnhBia"].Value.ToString();
            sach.NgayCapNhat = DateTime.Parse(dataGridView_Sach.CurrentRow.Cells["NgayCapNhat"].Value.ToString());
            sach.SoLuongTon = Int32.Parse(dataGridView_Sach.CurrentRow.Cells["SoLuongTon"].Value.ToString());
            sach.TenCD = dataGridView_Sach.CurrentRow.Cells["TenCD"].Value.ToString();
            sach.TenNXB = dataGridView_Sach.CurrentRow.Cells["TenNXB"].Value.ToString();


            if (dataGridView_Sach.CurrentRow.Cells[4].Value!=null && dataGridView_Sach.CurrentRow.Cells[4].Value.ToString()!=String.Empty)
            {
                sach.Moi = Int32.Parse(dataGridView_Sach.CurrentRow.Cells[4].Value.ToString());
            }
            if (dataGridView_Sach.CurrentRow.Cells[3].Value != null && dataGridView_Sach.CurrentRow.Cells[3].Value.ToString() != String.Empty)
            {
                sach.MoTa = dataGridView_Sach.CurrentRow.Cells[3].Value.ToString();
            }
            FormSuaSanPham sua = new FormSuaSanPham(sach, this);
            sua.Show();
            //MessageBox.Show(dataGridView_Sach.CurrentRow.Cells[9].Value.ToString());

        }

        private void btn_taodh_Click(object sender, EventArgs e)
        {
            FormTimKiem frm = new FormTimKiem(this);
            frm.Show();
        }

        private void btn_ThemSanPham_Click(object sender, EventArgs e)
        {
            FormThemSanPham form = new FormThemSanPham(this);
            form.Show();
        }

        private void btn_TimKiem_Click(object sender, EventArgs e)
        {
            List<ServiceKhachHang.KhachHang> kh = sv.FindKhachHangInTK(txt_TimKiem.Text).ToList() ;
            GridView_KhachHang.DataSource = null;
            GridView_KhachHang.DataSource = kh;
            CustomGrid();
        }

        private void btn_TimKiem_DH_Click(object sender, EventArgs e)
        {
            if(txt_TimKiem_MaDH.Text != String.Empty)
            {
                List<DonHangEntity> list_dh = dh.FindDonHangbyID(Int32.Parse(txt_TimKiem_MaDH.Text)).ToList();
                dataGridView_DonHang.DataSource = null;
                dataGridView_DonHang.DataSource = list_dh;
                CustomGrid();
               
            }
        }

        private void btn_TimKiem_TenSach_Click(object sender, EventArgs e)
        {
            List<Sach_NXB_CDEntity> list_sach = sach.SearchSach_NXB_CD(txt_TimKiem_TenSach.Text).ToList();

            dataGridView_Sach.DataSource = null;
           
            dataGridView_Sach.DataSource = list_sach;
            


        }
        public void CustomGrid()
        {
            // chỉnh sủa lại datagridview khách hàng

            GridView_KhachHang.Columns[0].Width = 80;
            GridView_KhachHang.Columns[0].HeaderCell.Value = "Mã KH";
            GridView_KhachHang.Columns[0].DataPropertyName = "MaKH";

            GridView_KhachHang.Columns["TaiKhoan"].Visible = false;
            GridView_KhachHang.Columns[1].HeaderCell.Value = "Tài Khoản";
            GridView_KhachHang.Columns[1].DataPropertyName = "TaiKhoan";
            GridView_KhachHang.Columns[1].Width = 120;

            GridView_KhachHang.Columns[2].HeaderCell.Value = "Mật Khẩu";
            GridView_KhachHang.Columns[2].DataPropertyName = "MatKhau";
            GridView_KhachHang.Columns[2].Width = 120;

            GridView_KhachHang.Columns[3].HeaderCell.Value = "Họ Tên";
            GridView_KhachHang.Columns[3].DataPropertyName = "HoTen";
            GridView_KhachHang.Columns[3].Width = 120;

            GridView_KhachHang.Columns[4].HeaderCell.Value = "Giới Tính";
            GridView_KhachHang.Columns[4].DataPropertyName = "GioiTinh";
            GridView_KhachHang.Columns[4].Width = 120;

            GridView_KhachHang.Columns[5].HeaderCell.Value = "Ngày Sinh";
            GridView_KhachHang.Columns[5].DataPropertyName = "NgaySinh";
            GridView_KhachHang.Columns[5].Width = 120;

            GridView_KhachHang.Columns[6].HeaderCell.Value = "Địa Chỉ";
            GridView_KhachHang.Columns[6].DataPropertyName = "DiaChi";
            GridView_KhachHang.Columns[6].Width = 120;

            GridView_KhachHang.Columns[7].HeaderCell.Value = "Email";
            GridView_KhachHang.Columns[7].DataPropertyName = "Email";
            GridView_KhachHang.Columns[7].Width = 120;

            GridView_KhachHang.Columns[8].HeaderCell.Value = "Điện Thoại";
            GridView_KhachHang.Columns[8].DataPropertyName = "DienThoai";
            GridView_KhachHang.Columns[8].Width = 140;

            // chỉnh sửa lại griddonhang
            dataGridView_DonHang.Columns[5].Width = 180;
            dataGridView_DonHang.Columns[6].Width = 180;
            dataGridView_DonHang.Columns["DonGia"].Width = 180;
        }

        private void dataGridView_Sach_MouseClick_1(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ContextMenuStrip menu = new ContextMenuStrip();
                int position_now = dataGridView_Sach.HitTest(e.X, e.Y).RowIndex;
                if (position_now >= 0)
                {

                    menu.Items.Add("Xóa").Name = "Xóa";
                }
                menu.Show(dataGridView_Sach, new Point(e.X, e.Y));
                menu.ItemClicked+=menu_ItemClicked;

            }
        }

        private void menu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            switch (e.ClickedItem.Name.ToString())
            {
                case "Sữa":

                    break;
                case "Xóa":
                    //MessageBox.Show(dataGridView_Sach.CurrentRow.Cells[2].Value.ToString());
                    if (sach.RemoveSach(Int32.Parse(dataGridView_Sach.CurrentRow.Cells[2].Value.ToString())))
                    {
                        MessageBox.Show("Xóa Thành Công !");
                        updateGrid();
                    }
                    else
                    {
                        MessageBox.Show("Xóa Thất Bại !");
                    }
                    break;
            }
        }

    }
}
