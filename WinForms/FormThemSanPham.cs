﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WinForms.ServiceSach;

namespace WinForms
{
    public partial class FormThemSanPham : Form
    {
        FormQuanLy _frm;
        SachServiceClient sach = new SachServiceClient();
        public FormThemSanPham(FormQuanLy __frm)
        {
            this._frm = __frm;
            InitializeComponent();
        }
     

        private void btn_browse_Click(object sender, EventArgs e)
        {
            int size = -1;
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            if (result == DialogResult.OK) // Test result.
            {
                string file = openFileDialog1.FileName;
                try
                {
                    string text = File.ReadAllText(file);
                    txt_AnhBia.Text = openFileDialog1.SafeFileName;
                    size = text.Length;
                }
                catch (IOException)
                {
                }
            }
        }

        private void FormThemSanPham_Load(object sender, EventArgs e)
        {


            //tạo list tên chủ đề
            List<string> list_CD = new List<string>();
            foreach (var item in sach.GetAllCD())
            {
                list_CD.Add(item.TenChuDe);
            }
            // tạo list tên nhà xuất bản

            List<string> list_NXB = new List<string>();
            foreach (var item in sach.GetAllNXB())
            {
                list_NXB.Add(item.TenNXB);
            }

            comboBox_CD.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_NXB.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_Moi.DropDownStyle = ComboBoxStyle.DropDownList;
            comboBox_CD.DataSource = list_CD;
            comboBox_NXB.DataSource = list_NXB;

          

        }

        private void btn_Them_Click(object sender, EventArgs e)
        {
            NhaXuatBan nxb = sach.MaNXB(comboBox_NXB.Text);
            ChuDe cd = sach.MaCD(comboBox_CD.Text);

            Sach sach_insert = new Sach();
            sach_insert.TenSach = txtTenSach.Text;
            sach_insert.GiaBan = Int32.Parse(txtGiaBan.Text);
            sach_insert.AnhBia = txt_AnhBia.Text;
            sach_insert.MoTa = richTextBox_MoTa.Text;
            sach_insert.Moi = Int32.Parse(comboBox_Moi.Text);
            sach_insert.NgayCapNhat = dateTimePicker_NgayCapNhat.Value;
            sach_insert.MaNXB = nxb.MaNXB;
            sach_insert.MaChuDe = cd.MaChuDe;
            sach_insert.SoLuongTon = Int32.Parse(txtSoLuongTon.Text);

            if (sach.InsertSach(sach_insert))
            {
                MessageBox.Show("Insert thành công !");
                this.Close();
                _frm.updateGrid();
            }
            else
            {
                MessageBox.Show("Insert thất bại !");
            }


            //lấy vị trí file
            string fileLocation = openFileDialog1.FileName;
            if (fileLocation != "openFileDialog1") // Test result.
            {
                //lấy đường dẫn lưu
                var path = Path.GetFullPath("../../../ClientWebsite/HinhAnhSP");
                //kiểm tra ảnh tồn tại chưa
                //copy file ảnh vào thư mục
                File.Copy(fileLocation, Path.Combine(path, Path.GetFileName(fileLocation)), true);
            }
        }
    }
}
