﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BanSachService.Model;

namespace BanSachService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "NguoiDungService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select NguoiDungService.svc or NguoiDungService.svc.cs at the Solution Explorer and start debugging.
    public class NguoiDungService : INguoiDungService
    {
        BanSach_Entities db = new BanSach_Entities();
        public KhachHang getKhachHang(string TaiKhoan)
        {
            return db.KhachHangs.SingleOrDefault(n => n.TaiKhoan == TaiKhoan);
             
        }

        public List<KhachHang> getListKhachHang()
        {
            return db.KhachHangs.ToList();
        }

        public bool AddKhachHang(KhachHang kh)
        {
            try
            {
                db.KhachHangs.Add(kh);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool EditKhachHang(KhachHang kh)
        {
            try
            {
                db.Entry(kh).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool RemoveKhachHang(int id)
        {
            try
            {
                KhachHang kh = db.KhachHangs.SingleOrDefault(t => t.MaKH == id);
                db.KhachHangs.Remove(kh);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public int AddKhachHangID(KhachHang kh)
        {
            db.KhachHangs.Add(kh);
            db.SaveChanges();
            return kh.MaKH;
        }

        public List<KhachHang> FindKhachHangInTK(string TaiKhoan)
        {
            return db.KhachHangs.Where(n => n.TaiKhoan.Contains(TaiKhoan)).ToList();
        }

        public KhachHang getKHLogin(string sTaiKhoan, string sMatKhau)
        {
            return db.KhachHangs.Where(n => n.TaiKhoan == sTaiKhoan && n.MatKhau == sMatKhau).SingleOrDefault();
        }

        public KhachHang getKhachHangbyID(int Id)
        {
            return db.KhachHangs.Where(n => n.MaKH == Id).SingleOrDefault();
        }
    }
}
