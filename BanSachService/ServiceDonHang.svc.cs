﻿using BanSachService.Constructor;
using BanSachService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BanSachService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServiceDonHang" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServiceDonHang.svc or ServiceDonHang.svc.cs at the Solution Explorer and start debugging.
    public class ServiceDonHang : IServiceDonHang
    {
        BanSach_Entities db = new BanSach_Entities();
        public List<DonHangEntity> getAllDonHang()
        {
            var query = (from a in db.DonHangs
                           join c in db.ChiTietDonHangs on a.MaDonHang equals c.MaDonHang
                           //where c.ClientID == yourDescriptionObject.ClientID
                           select new DonHangEntity()
                           {
                               MaKH = a.MaKH,
                               MaDonHang =a.MaDonHang,
                               MaSach =c.MaSach,
                               SoLuong= c.SoLuong,
                               DonGia=c.DonGia,
                               NgayDat= a.NgayDat,
                               NgayGiao=a.NgayGiao,
                               DaThanhToan=a.DaThanhToan,
                               TinhTrangGiaoHang = a.TinhTrangGiaoHang
                           }).ToList();
            return query;
        }



        

        public bool AddCTDonHang(ChiTietDonHang ctdh)
        {
            try
            {
                db.ChiTietDonHangs.Add(ctdh);
                db.SaveChanges();
                return true;

            }
            catch
            {
                return false;
            }
        }


        public int AddDonHang(DonHang dh)
        {
            db.DonHangs.Add(dh);
            db.SaveChanges();
            return dh.MaDonHang;
        }

        public List<DonHangEntity> FindDonHangbyID(int ID)
        {
            var query = (from a in db.DonHangs
                         join c in db.ChiTietDonHangs on a.MaDonHang equals c.MaDonHang
                         where(a.MaDonHang== ID)
                         select new DonHangEntity()
                         {
                             MaKH = a.MaKH,
                             MaDonHang = a.MaDonHang,
                             MaSach = c.MaSach,
                             SoLuong = c.SoLuong,
                             DonGia = c.DonGia,
                             NgayDat = a.NgayDat,
                             NgayGiao = a.NgayGiao,
                             DaThanhToan = a.DaThanhToan,
                             TinhTrangGiaoHang = a.TinhTrangGiaoHang
                         }).ToList();
            return query;
        }

        public bool RemoveDH(int MaDH)
        {
            try
            {
                DonHang dh = db.DonHangs.Where(n => n.MaDonHang == MaDH).SingleOrDefault();
                db.DonHangs.Remove(dh);
                return true;
            }
            catch
            { return false; }
        }
    }
}
