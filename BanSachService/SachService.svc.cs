﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using BanSachService.Model;
using BanSachService.Constructor;

namespace BanSachService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SachService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SachService.svc or SachService.svc.cs at the Solution Explorer and start debugging.
    public class SachService : ISachService
    {
        BanSach_Entities db = new BanSach_Entities();

        public List<ChuDe> GetAllCD()
        {
            return db.ChuDes.ToList();
        }

        public List<NhaXuatBan> GetAllNXB()
        {
            return db.NhaXuatBans.ToList();
        }

        public List<Sach> getAllSach_Page()
        {
            return db.Saches.ToList();
        }

        public List<Sach_NXB_CDEntity> GetAllSach()
        {
            var query = (from a in db.Saches
                         join c in db.ChuDes on a.MaChuDe equals c.MaChuDe
                         join d in db.NhaXuatBans on a.MaNXB equals d.MaNXB
                         //where c.ClientID == yourDescriptionObject.ClientID
                         select new Sach_NXB_CDEntity()
                         {
                             MaSach = a.MaSach,
                             TenSach = a.TenSach,
                             GiaBan = a.GiaBan,
                             MoTa = a.MoTa,
                             AnhBia = a.AnhBia,
                             NgayCapNhat = a.NgayCapNhat,
                             SoLuongTon = a.SoLuongTon,
                             Moi = a.Moi,
                             TenCD = c.TenChuDe,
                             TenNXB = d.TenNXB

                         }).ToList();
            return query;
         

        }

        public bool InsertSach(Sach sach)
        {
            try
            {
                db.Saches.Add(sach);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public ChuDe MaCD(string name)
        {
            return db.ChuDes.Where(n => n.TenChuDe == name).SingleOrDefault();
        }

        public NhaXuatBan MaNXB(string name)
        {
            return db.NhaXuatBans.Where(n => n.TenNXB == name).SingleOrDefault();
        }

        public List<Sach> SearchSach(string TuKhoa)
        {
            List<Sach> lstKQTK = db.Saches.Where(n => n.TenSach.Contains(TuKhoa)).ToList();


            return lstKQTK;
        }

        public List<Sach_NXB_CDEntity> SearchSach_NXB_CD(string TuKhoa)
        {
            var query = (from a in db.Saches
                         join c in db.ChuDes on a.MaChuDe equals c.MaChuDe
                         join d in db.NhaXuatBans on a.MaNXB equals d.MaNXB
                         where a.TenSach.Contains(TuKhoa)
                         select new Sach_NXB_CDEntity()
                         {
                             MaSach = a.MaSach,
                             TenSach = a.TenSach,
                             GiaBan = a.GiaBan,
                             MoTa = a.MoTa,
                             AnhBia = a.AnhBia,
                             NgayCapNhat = a.NgayCapNhat,
                             SoLuongTon = a.SoLuongTon,
                             Moi = a.Moi,
                             TenCD = c.TenChuDe,
                             TenNXB = d.TenNXB

                         }).ToList();
            return query;

        }

        public bool UpdateSach(Sach sach)
        {
            try
            {
                db.Entry(sach).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public List<Sach> GetTake3Sach()
        {
            return db.Saches.Take(3).ToList();
        }

        public List<ChuDe> getTake8ChuDe()
        {
            return db.ChuDes.Take(8).ToList();
        }

        public Sach GetSachById(int MaSach)
        {
            return db.Saches.Where(n => n.MaSach == MaSach).SingleOrDefault();
        }

        public ChuDe GetChuDeById(int MaChuDe)
        {
            return db.ChuDes.Where(n => n.MaChuDe == MaChuDe).SingleOrDefault();
        }

        public NhaXuatBan GetNXBByID(int MaNXB)
        {
            return db.NhaXuatBans.Where(n => n.MaNXB == MaNXB).SingleOrDefault();
        }

        public List<Sach> GetSachByMoi()
        {
            return db.Saches.Where(n => n.Moi == 1).ToList();
        }

        public List<Sach> GetSachByMaCD(int MaChuDe)
        {
            return db.Saches.Where(n => n.MaChuDe == MaChuDe).ToList();
        }

        public List<ChuDe> SearchCD(string sTenCD)
        {
            return db.ChuDes.Where(n => n.TenChuDe.Contains(sTenCD)).ToList();
        }

        public List<NhaXuatBan> SearchNXB(string sTenNXB)
        {
            return db.NhaXuatBans.Where(n => n.TenNXB.Contains(sTenNXB)).ToList();
        }

        public List<TacGia> SearchTG(string sTenTG)
        {
            return db.TacGias.Where(n => n.TenTacGia.Contains(sTenTG)).ToList();
        }

        public List<TacGia> getTake4TG()
        {
            return db.TacGias.Take(4).ToList();
        }

        public List<NhaXuatBan> getTake4NXB()
        {
            return db.NhaXuatBans.Take(4).ToList();
        }

        public List<TacGia> GetAllTG()
        {
            return db.TacGias.ToList();
        }

        public List<Sach> GetSachByIdTG(int MaTG)
        {
            return null;
        }

        public List<Sach> GetSachByNXB(int MaNXB)
        {
            return db.Saches.Where(n => n.MaNXB == MaNXB).ToList();
        }


        public bool RemoveSach(int MaSach)
        {
            try
            {
                Sach sach = db.Saches.SingleOrDefault(n=>n.MaSach == MaSach);
                db.Saches.Remove(sach);
                db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
