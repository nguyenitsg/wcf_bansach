﻿using BanSachService.Constructor;
using BanSachService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BanSachService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISachService" in both code and config file together.
    [ServiceContract]
    public interface ISachService
    {
        // tìm kiếm sách theo từ khóa
        [OperationContract]
        List<Sach> SearchSach(string TuKhoa);
        // lấy tất cả sách có loại là mới
        [OperationContract]
        List<Sach> GetSachByMoi();
        // lấy sách theo chủ đề 
        [OperationContract]
        List<Sach> GetSachByMaCD(int MaChuDe);
        // lấy ra 4 tác giả 
        [OperationContract]
        List<TacGia> getTake4TG();
        // lấy ra 4 tác nxb
        [OperationContract]
        List<NhaXuatBan> getTake4NXB();
        // tìm kiếm chủ đề
        [OperationContract]
        List<ChuDe> SearchCD(string sTenCD);
        // lấy tất cả các sách
        [OperationContract]
        List<Sach_NXB_CDEntity> GetAllSach();
        // lấy tất  chủ đề
        [OperationContract]
        List<ChuDe> GetAllCD();
        // tìm kiếm tên NXB
        [OperationContract]
        List<NhaXuatBan> SearchNXB(string sTenNXB);
        // Tìm kiếm theo tác giả
        [OperationContract]
        List<TacGia> SearchTG(string sTenTG);
        // lấy tất cả  NXB
        [OperationContract]
        List<NhaXuatBan> GetAllNXB();
        // lấy nxb theo tên
        [OperationContract]
        NhaXuatBan MaNXB(string name);
        // lấy chủ đề theo tên
        [OperationContract]
        ChuDe MaCD(string name);
        // update sách
        [OperationContract]
        Boolean UpdateSach(Sach sach);
        // thêm sách
        [OperationContract]
        Boolean InsertSach(Sach sach);
        // tìm kiếm sách
        [OperationContract]
        List<Sach_NXB_CDEntity> SearchSach_NXB_CD(string TuKhoa);
        // lấy tất cả sách 
        [OperationContract]
        List<Sach> getAllSach_Page();

        // lấy sách theo id
        [OperationContract]
        Sach GetSachById(int MaSach);

        // lấy 3 sách mới nhất 
        [OperationContract]
        List<Sach> GetTake3Sach();
        //Lấy ra 8 chủ đề đầu
        [OperationContract]
        List<ChuDe> getTake8ChuDe();
        // lấy chủ đề theo mã chủ đề
        [OperationContract]
        ChuDe GetChuDeById(int MaChuDe);
        // lấy nhà xuất bản theo mã
        [OperationContract]
        NhaXuatBan GetNXBByID(int MaNXB);
        // lấy list tac gia
        [OperationContract]
        List<TacGia> GetAllTG();
        // lấy sách theo mã TG
        [OperationContract]
        List<Sach> GetSachByIdTG(int MaTG);
        // lấy sách theo mã nxb
        [OperationContract]
        List<Sach> GetSachByNXB(int MaNXB);
        // xóa sách theo mã sách
        [OperationContract]
        Boolean RemoveSach(int MaSach);


    }
}
