﻿using BanSachService.Constructor;
using BanSachService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BanSachService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IServiceDonHang" in both code and config file together.
    [ServiceContract]
    public interface IServiceDonHang
    {
        //Lấy tất cả đơn hàng
        [OperationContract]
        List<DonHangEntity> getAllDonHang();

        // Thêm đơn hang
        [OperationContract]
        int AddDonHang(DonHang dh);
        // Thêm CTD đơn hang
        [OperationContract]
        Boolean AddCTDonHang(ChiTietDonHang ctdh);
        //Tìm kiếm đơn hàng theo mã
        [OperationContract]
        List<DonHangEntity> FindDonHangbyID(int ID);
        // Xóa Đơn hàng theo mã ĐH
        [OperationContract]
        Boolean RemoveDH(int MaDH);

    }
}
