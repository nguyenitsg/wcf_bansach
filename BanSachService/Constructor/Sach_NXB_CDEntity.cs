﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BanSachService.Constructor
{
    public class Sach_NXB_CDEntity
    {
        public int MaSach { get; set; }
        public string TenSach { get; set; }
        public Nullable<decimal> GiaBan { get; set; }
        public string MoTa { get; set; }
        public Nullable<System.DateTime> NgayCapNhat { get; set; }
        public string AnhBia { get; set; }
        public Nullable<int> SoLuongTon { get; set; }
        public string TenCD { get; set; }
        public string TenNXB { get; set; }
        public Nullable<int> Moi { get; set; }
    }
}