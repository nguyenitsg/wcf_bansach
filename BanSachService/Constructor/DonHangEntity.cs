﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace BanSachService.Constructor
{
    [DataContract]
    public class DonHangEntity
    {
        public DonHangEntity()
        {

        }
        [DataMember]
        public int MaDonHang { get; set; }
        [DataMember]
        public int MaSach { get; set; }
        [DataMember]
        public Nullable<int> SoLuong { get; set; }
        [DataMember]
        public Nullable<decimal> DonGia { get; set; }
        [DataMember]
        public Nullable<System.DateTime> NgayGiao { get; set; }
        [DataMember]
        public Nullable<System.DateTime> NgayDat { get; set; }
        [DataMember]
        public string DaThanhToan { get; set; }
        [DataMember]
        public Nullable<int> TinhTrangGiaoHang { get; set; }
        [DataMember]
        public Nullable<int> MaKH { get; set; }
    }
}