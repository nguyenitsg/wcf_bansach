﻿using BanSachService.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace BanSachService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "INguoiDungService" in both code and config file together.
    [ServiceContract]
    public interface INguoiDungService
    {
        //Lấy khách hàng theo mã khách hàng
        [OperationContract]
        KhachHang getKhachHangbyID(int Id);
        // lấy 1 khách hàng theo tài khoản
        [OperationContract]
        KhachHang getKhachHang(string TaiKhoan);
        //Lấy tất cả các khách hàng
        [OperationContract]
        List<KhachHang> getListKhachHang();
        // lấy  tài khoản để đăng nhập
        [OperationContract]
        KhachHang getKHLogin(string sTaiKhoan, string sMatKhau);
        //Thêm mới khách hàng
        [OperationContract]
        Boolean AddKhachHang(KhachHang kh);
        //Sữa khách hàng 
        [OperationContract]
        Boolean EditKhachHang(KhachHang kh);
        // Xóa Khách Hàng
        [OperationContract]
        Boolean RemoveKhachHang(int id);
        
        [OperationContract]
        int AddKhachHangID(KhachHang kh);

        [OperationContract]
        List<KhachHang> FindKhachHangInTK(string TaiKhoan);
    }
}
